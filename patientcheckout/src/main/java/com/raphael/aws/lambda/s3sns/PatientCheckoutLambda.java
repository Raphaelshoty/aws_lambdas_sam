package com.raphael.aws.lambda.s3sns;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PatientCheckoutLambda {
	
	private final AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();
	private final ObjectMapper mapper = new ObjectMapper();
	private final AmazonSNS sns = AmazonSNSAsyncClientBuilder.defaultClient();
	private static final String PATIENT_CHECKOUT_TOPIC = System.getenv("PATIENT_CHECKOUT_TOPIC");
		
	public void handler(S3Event event, Context context) throws InterruptedException {
		
		Thread.sleep(5000);
		
		//LambdaLogger logger =  context.getLogger();
		Logger logger = LoggerFactory.getLogger(PatientCheckoutLambda.class);
		
		event.getRecords().forEach(record -> {
			S3ObjectInputStream s3InputStream = s3
					.getObject(record.getS3().getBucket().getName(), record.getS3().getObject().getKey())
					.getObjectContent();				
			try {
			logger.info("Reading data from S3");
			List<PatientCheckoutEvent> patientCheckoutEvents = Arrays.asList(mapper.readValue(s3InputStream, PatientCheckoutEvent[].class)) ;
			logger.info(patientCheckoutEvents.toString());
			s3InputStream.close();
			
			logger.info("Publishing message on SNS");
			publishToSNS(patientCheckoutEvents);
			
			}
			catch(JsonParseException e) {
				e.printStackTrace();
			} catch(JsonMappingException e) {
				logger.error("Exception is: ", e);
				throw new RuntimeException("Error on S3 Event Processing", e);
			}
			catch (IOException e) {
//				StringWriter stringWriter = new StringWriter();
//				
//				e.printStackTrace(new PrintWriter(stringWriter));
//				
//				logger.log(stringWriter.toString()); // this logger is for LambdaLogger
				logger.error("Exception is: ", e);
			} 
		});
	}

	private void publishToSNS(List<PatientCheckoutEvent> patientCheckoutEvents) {
		patientCheckoutEvents.forEach(checkoutEvent->{
			try {						
					sns.publish(PATIENT_CHECKOUT_TOPIC, mapper.writeValueAsString(checkoutEvent));
			} catch (JsonProcessingException e) {				
				e.printStackTrace();
			}
		});
	}

}
