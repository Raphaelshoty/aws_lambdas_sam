package com.raphael.aws.lambda.s3sns.errorhandling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.events.SNSEvent;

public class ErrorHandler {		
	
	private final Logger logger = LoggerFactory.getLogger(ErrorHandler.class);	
	
	public void handler(SNSEvent event) {				
		event.getRecords().forEach(record->	logger.info("Dead Letter Queue Event " + record.toString() ));
	}

}
 